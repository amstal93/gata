describe("whenScrolledBottom (only shallow tests)", function () {
  beforeEach(module('gataApp'));

  var $rootScope, $compile;

  beforeEach(inject(function($injector) {
    $rootScope = $injector.get('$rootScope');
    $compile = $injector.get('$compile');
  }));

  afterEach(function() {
  });

  it("Calls whenScrolledBottom when hit bottom", function () {
    expect(true).toBe(true);
    var scope = $rootScope.$new();
    scope.whenScrolledBottom = function() {};
    spyOn(scope, 'whenScrolledBottom');
    var element = $compile("<div when-scrolled-bottom='whenScrolledBottom()' />")(scope);
    element.triggerHandler("scroll");
    expect(scope.whenScrolledBottom).toHaveBeenCalled();
  });

  it("Does not call whenScrolledBottom when does not hit bottom", function () {
    expect(true).toBe(true);
    var scope = $rootScope.$new();
    scope.whenScrolledBottom = function() {};
    spyOn(scope, 'whenScrolledBottom');
    var element = $compile("<div when-scrolled-bottom='whenScrolledBottom()' when-scrolled-bottom-margin='-1' />")(scope);
    element.triggerHandler("scroll");
    expect(scope.whenScrolledBottom).not.toHaveBeenCalled();
  });

  it("Does not call whenScrolledBottom if disabled", function () {
    expect(true).toBe(true);
    var scope = $rootScope.$new();
    scope.whenScrolledBottom = function() {};
    var whenScrolledBottomSpy = spyOn(scope, 'whenScrolledBottom');
    scope.disabled = true;
    var element = $compile("<div when-scrolled-bottom='whenScrolledBottom()' when-scrolled-disabled='disabled'/>")(scope);
    element.triggerHandler("scroll");
    expect(scope.whenScrolledBottom).not.toHaveBeenCalled();
  });

});
