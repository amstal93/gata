package com.odroid.gata.integration.web;

import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor;

import static com.odroid.gata.integration.IntegrationTestUtils.USERNAME_ADMIN;
import static com.odroid.gata.integration.IntegrationTestUtils.USERNAME_USER;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

public abstract class WebIntegrationTestUtils {

    public static final String USERNAME_PARAM = "username";
    public static final String PASSWORD_PARAM = "pass" + "word";
    public static final String USERNAME_ADMIN_OK = USERNAME_ADMIN;
    public static final String PASSWRD_ADMIN_OK = "a";
    public static final String PASSWRD_ADMIN_KO = "aa";
    public static final String USERNAME_USER_OK = USERNAME_USER;
    public static final String PASSWRD_USER_OK = "b";
    public static final String PASSWRD_USER_KO = "bb";
    private static final String ROLE_ADMIN_SHORT = "ADMIN";
    private static final String ROLE_USER_SHORT = "USER";
    public static final String LOGOUT_TEXT = "Logout ";
    public static final String LOGIN_URL = "/login";
    public static final String LOGOUT_URL = "/logout";
    public static final String NOTESTAB_URL = "/notesTab";
    public static final String USERSTAB_URL = "/usersTab";
    private static final String RES_URL = "/res";
    public static final String FAVICON_URL = RES_URL + "/favicon.png";
    private static final String WEBJARS_URL = "/webjars";
    public static final String ANGULARJS_URL = WEBJARS_URL + "/angularjs/angular.js";

    private WebIntegrationTestUtils() {
        // Intentionally blank to avoid instantiation
    }

    public static UserRequestPostProcessor testAdmin() {
        return user(USERNAME_ADMIN_OK)
                .password(PASSWRD_ADMIN_OK)
                .roles(ROLE_ADMIN_SHORT, ROLE_USER_SHORT);
    }

    public static UserRequestPostProcessor testUser() {
        return user(USERNAME_USER_OK)
                .password(PASSWRD_USER_OK)
                .roles(ROLE_USER_SHORT);
    }

}
