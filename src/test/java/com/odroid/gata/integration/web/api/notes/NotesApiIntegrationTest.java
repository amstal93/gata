package com.odroid.gata.integration.web.api.notes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odroid.gata.SpringBootApp;
import com.odroid.gata.service.iface.notes.DetailedNote;
import com.odroid.gata.service.iface.notes.MinimalNote;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.odroid.gata.integration.IntegrationTestUtils.ID_ADMIN;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.testAdmin;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.testUser;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class,
        properties = {
                "logging.level.org.springframework.test.web.servlet.result=DEBUG"
        })
@TestPropertySource("classpath:test.properties")
public class NotesApiIntegrationTest {

    private static final String NOTES_URL = "/notes";
    private static final String NOTES_FILTER_PARAM = "filter";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    private MockMvc mockMvc;

    private ObjectMapper jackson = new ObjectMapper();

    @Before
    public void configureMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void findMyNotesWithoutFilter() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                get(NOTES_URL).with(testAdmin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
        List<MinimalNote> minimalNotes = jackson.readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<List<MinimalNote>>() {
                });
        assertEquals(0, minimalNotes.size());
    }

    @Test
    public void findMyNotesWithEmptyFilter() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                get(NOTES_URL).param(NOTES_FILTER_PARAM, "").with(testAdmin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
        List<MinimalNote> minimalNotes = jackson.readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<List<MinimalNote>>() {
                });
        assertEquals(0, minimalNotes.size());
    }

    @Test
    public void findMyNotesWithFilter() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                get(NOTES_URL).param(NOTES_FILTER_PARAM, "a").with(testAdmin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
        List<MinimalNote> minimalNotes = jackson.readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<List<MinimalNote>>() {
                });
        assertEquals(0, minimalNotes.size());
    }

    @Test
    public void testGetNote() throws Exception {
        String title = "title";
        String content = "content";
        Long noteId = createNote(title, content, ID_ADMIN);
        try {
            MvcResult mvcResult = mockMvc.perform(
                    get(NOTES_URL + "/{id}", noteId).with(testAdmin()))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                    .andReturn();
            DetailedNote note = jackson.readValue(
                    mvcResult.getResponse().getContentAsString(),
                    DetailedNote.class);
            DetailedNote expectedNote = new DetailedNote(noteId, title, content);
            assertEquals(expectedNote, note);
        } finally {
            deleteNote(noteId);
        }
    }

    @Test
    public void testGetNoteNotExist() throws Exception {
        String title = "title";
        String content = "content";
        Long noteId = createNote(title, content, ID_ADMIN);
        try {
            mockMvc.perform(
                    get(NOTES_URL + "/{id}", (noteId + 1)).with(testAdmin()))
                    .andExpect(status().isBadRequest());
        } finally {
            deleteNote(noteId);
        }
    }

    @Test
    public void testGetNoteHacker() throws Exception {
        String title = "title";
        String content = "content";
        Long noteId = createNote(title, content, ID_ADMIN);
        try {
            mockMvc.perform(
                    get(NOTES_URL + "/{id}", noteId).with(testUser()))
                    .andExpect(status().isBadRequest());
        } finally {
            deleteNote(noteId);
        }
    }

    @Test
    public void testCreateNote() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                post(NOTES_URL).with(testAdmin()).with(csrf()))
                .andExpect(status().isOk())
                .andReturn();
        Long noteId = jackson.readValue(
                mvcResult.getResponse().getContentAsString(),
                Long.class);
        try {
            DetailedNote createdNote = getNote(noteId);
            DetailedNote expectedNote = new DetailedNote(noteId, "Lorem ipsum", "dolor sit amet");
            assertEquals(expectedNote, createdNote);
        } finally {
            deleteNote(noteId);
        }
    }

    @Test
    public void testUpdateNote() throws Exception {
        Long noteId = createNote("title", "content", ID_ADMIN);
        try {
            String newTitle = "new title";
            String newContent = "new content";
            DetailedNote noteToUpdate = new DetailedNote(noteId, newTitle, newContent);
            mockMvc.perform(
                    put(NOTES_URL).with(testAdmin()).with(csrf())
                            .contentType(APPLICATION_JSON_UTF8_VALUE)
                            .content(jackson.writeValueAsString(noteToUpdate)))
                    .andExpect(status().isOk());
            DetailedNote updatedNote = getNote(noteId);
            assertEquals(noteToUpdate, updatedNote);
        } finally {
            deleteNote(noteId);
        }

    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testDeleteNote() throws Exception {
        Long noteId = createNote("title", "content", ID_ADMIN);
        try {
            mockMvc.perform(
                    delete(NOTES_URL + "/{id}", noteId).with(testAdmin()).with(csrf()))
                    .andExpect(status().isOk());
            getNote(noteId);
        } finally {
            deleteNote(noteId);
        }
    }

    private Long createNote(String title, String content, Long userId) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                "INSERT INTO notes (title, content, user_id) VALUES (:title, :content, :userId)",
                new MapSqlParameterSource()
                        .addValue("title", title)
                        .addValue("content", content)
                        .addValue("userId", userId),
                holder);
        return holder.getKey().longValue();
    }

    private void deleteNote(Long id) {
        jdbcTemplate.update(
                "DELETE FROM notes WHERE id = :id",
                new MapSqlParameterSource("id", id));
    }

    private DetailedNote getNote(Long id) {
        return jdbcTemplate.queryForObject(
                "SELECT id, title, content FROM notes WHERE id = :id",
                new MapSqlParameterSource("id", id),
                new BeanPropertyRowMapper<>(DetailedNote.class)
        );
    }

}
