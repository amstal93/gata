package com.odroid.gata.integration.jpa;

import com.odroid.gata.SpringBootApp;
import com.odroid.gata.domain.Authority;
import com.odroid.gata.domain.User;
import com.odroid.gata.domain.UserAuthority;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class)
@TestPropertySource("classpath:test.properties")
public class UserAuthorityJpaIntegrationTest {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(UserAuthorityJpaIntegrationTest.class);

    private static final String AUSER_USERNAME = "a";

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Test
    @Transactional
    public void testMinimalAdmins() {
        List<UserAuthority> adminsUserAuthorities = em.createQuery(
                "from UserAuthority a where a.authority = :authority", UserAuthority.class)
                .setParameter("authority", Authority.ROLE_ADMIN)
                .getResultList();
        LOGGER.debug("Admins: {}", adminsUserAuthorities);
        assertTrue(
                adminsUserAuthorities.stream().map(UserAuthority::getUser).map(User::getUsername)
                        .anyMatch(AUSER_USERNAME::equals));
    }

    @Test
    @Transactional
    public void testAuthority() {
        User xUser = new User("x", "pwd", true, "Xp");
        em.persist(xUser);
        xUser.getUserAuthorities().add(new UserAuthority(Authority.ROLE_USER, xUser));
        em.flush();
        String role = namedParameterJdbcTemplate.queryForObject(
                "SELECT authority FROM user_authorities WHERE username = 'x'",
                new EmptySqlParameterSource(),
                String.class
        );
        assertEquals(Authority.ROLE_USER.name(), role);
    }

}
