package com.odroid.gata.integration.db;

import com.odroid.gata.SpringBootApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class)
@TestPropertySource("classpath:test.properties")
public class DbIntegrationTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void testMinimalDbExists() {
        int aUserCount = jdbcTemplate.queryForObject(
                "SELECT COUNT(*) FROM users WHERE username = 'a'", Integer.class);
        assertEquals(1, aUserCount);
        int bUserCount = jdbcTemplate.queryForObject(
                "SELECT COUNT(*) FROM users WHERE username = 'b'", Integer.class);
        assertEquals(1, bUserCount);
        int authoritiesCount = jdbcTemplate.queryForObject(
                "SELECT COUNT(*) FROM user_authorities", Integer.class);
        assertEquals(3, authoritiesCount);
        int notesCount = jdbcTemplate.queryForObject(
                "SELECT COUNT(*) FROM notes", Integer.class);
        assertTrue(notesCount >= 0);
    }

    @Test
    public void testIdentity() {
        try {
            jdbcTemplate.update(
                    "INSERT INTO users (username, hash, enabled, name) VALUES ('x', 'xp', 1, 'X X')");
            int xUserCount = jdbcTemplate.queryForObject(
                    "SELECT COUNT(*) FROM users WHERE username = 'x'", Integer.class);
            assertEquals(1, xUserCount);
        } finally {
            jdbcTemplate.update("DELETE FROM users WHERE username = 'x'");
        }
    }

}
