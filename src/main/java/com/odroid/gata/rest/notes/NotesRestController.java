package com.odroid.gata.rest.notes;

import com.odroid.gata.common.GataConfiguration;
import com.odroid.gata.service.iface.notes.DetailedNote;
import com.odroid.gata.service.iface.notes.MinimalNote;
import com.odroid.gata.service.iface.notes.NotesService;

import java.security.Principal;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotesRestController {

    private static final String NOTES_URL = "/notes";

    private NotesService notesService;
    private GataConfiguration gataConfiguration;

    public NotesRestController(NotesService notesService, GataConfiguration gataConfiguration) {
        this.notesService = notesService;
        this.gataConfiguration = gataConfiguration;
    }

    @GetMapping(value = NOTES_URL, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public List<MinimalNote> find(
            @RequestParam(value = "filter", defaultValue = "") String filter,
            @RequestParam(value = "from", defaultValue = "0") long from,
            Principal principal
    ) {
        return notesService.findNotes(filter, principal.getName(), from, gataConfiguration.getPageSize());
    }

    @GetMapping(value = NOTES_URL + "/{id}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public DetailedNote retrieve(@PathVariable("id") Long id, Principal principal) {
        return notesService.getNote(id, principal.getName());
    }

    @PostMapping(value = NOTES_URL, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public Long create(Principal principal) {
        return notesService.createNote(principal.getName());
    }

    @PutMapping(value = NOTES_URL, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public void update(@RequestBody DetailedNote note, Principal principal) {
        notesService.updateNote(note, principal.getName());
    }

    @DeleteMapping(value = NOTES_URL + "/{id}")
    public void delete(@PathVariable("id") Long id, Principal principal) {
        notesService.deleteNote(id, principal.getName());
    }

}
