package com.odroid.gata.service.impl.notes;


import com.odroid.gata.domain.Note;
import com.odroid.gata.domain.User;
import com.odroid.gata.repository.NotesRepository;
import com.odroid.gata.repository.UsersRepository;
import com.odroid.gata.service.iface.notes.DetailedNote;
import com.odroid.gata.service.iface.notes.MinimalNote;
import com.odroid.gata.service.iface.notes.NotesService;
import com.odroid.gata.service.impl.ForbiddenException;
import com.odroid.gata.service.impl.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class NotesServiceJpaImpl implements NotesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotesServiceJpaImpl.class);
    public static final String NOTE = "Note";

    private NotesRepository notesRepository;

    private UsersRepository usersRepository;

    public NotesServiceJpaImpl(NotesRepository notesRepository, UsersRepository usersRepository) {
        this.notesRepository = notesRepository;
        this.usersRepository = usersRepository;
    }

    @Override
    public List<MinimalNote> findNotes(String filter, String username, long from, long limit) {
        LOGGER.debug("Filtering {} notes from {} for {} with {}", limit, from, username, filter);
        List<MinimalNote> notes = notesRepository
                .findNotes(filter, username)
                .skip(from)
                .limit(limit)
                .map(MinimalNote::fromNote)
                .collect(Collectors.toList());
        LOGGER.debug("Filtered notes: {}", notes);
        return notes;
    }

    @Override
    public DetailedNote getNote(Long id, String username) {
        LOGGER.debug("Getting note {} for {}", id, username);
        DetailedNote note = DetailedNote.fromNote(getNoteOwnedBy(id, username));
        LOGGER.debug("Got note: {}", note);
        return note;
    }

    private Note getNoteOwnedBy(Long id, String username) {
        Note note = notesRepository.findById(id).orElseThrow(() -> new NotFoundException(NOTE, id));
        if (!note.getUser().getUsername().equals(username)) {
            throw new ForbiddenException();
        }
        return note;
    }

    @Override
    @Transactional
    public Long createNote(String username) {
        LOGGER.debug("Creating note for {}", username);
        User user = usersRepository.findByUsername(username);
        Note note = new Note("Lorem ipsum", "dolor sit amet", user);
        Note savedNote = notesRepository.save(note);
        Long noteId = savedNote.getId();
        LOGGER.debug("Created note {}", noteId);
        return noteId;
    }

    @Override
    @Transactional
    public void updateNote(DetailedNote detailedNote, String username) {
        LOGGER.debug("Updating note {} for {}", detailedNote, username);
        Note note = getNoteOwnedBy(detailedNote.getId(), username);
        note.setTitle(detailedNote.getTitle());
        note.setContent(detailedNote.getContent());
        LOGGER.debug("Updating note {}", detailedNote);
    }

    @Transactional
    public void deleteNote(Long id, String username) {
        LOGGER.debug("Deleting note {} for {}", id, username);
        Note note = getNoteOwnedBy(id, username);
        notesRepository.delete(note);
        LOGGER.debug("Deleted note {}", id);
    }


}
