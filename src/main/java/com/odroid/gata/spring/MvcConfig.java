package com.odroid.gata.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.context.ThemeSource;
import org.springframework.ui.context.support.ResourceBundleThemeSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ThemeResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.theme.CookieThemeResolver;
import org.springframework.web.servlet.theme.ThemeChangeInterceptor;

import java.util.Locale;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    private static final String LOGIN_VIEW = "login";
    public static final String LOGIN_URL = "/" + LOGIN_VIEW;
    private static final String NOTES_VIEW = "notesTab";
    public static final String NOTESTAB_URL = "/" + NOTES_VIEW;
    private static final String USERS_VIEW = "usersTab";
    private static final String USERSTAB_URL = "/" + USERS_VIEW;

    private static final String DEFAULT_THEME_NAME = "pink";
    private static final String THEMES_BASENAME_PREFIX = "themes/";

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController(LOGIN_URL).setViewName(LOGIN_VIEW);
        registry.addViewController(NOTESTAB_URL).setViewName(NOTES_VIEW);
        registry.addViewController(USERSTAB_URL).setViewName(USERS_VIEW);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ThemeChangeInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new LocaleChangeInterceptor()).addPathPatterns("/**");
    }

    @Bean
    public ThemeResolver themeResolver() {
        CookieThemeResolver cookieThemeResolver = new CookieThemeResolver();
        cookieThemeResolver.setDefaultThemeName(DEFAULT_THEME_NAME);
        return cookieThemeResolver;
    }

    @Bean
    public ThemeSource themeSource() {
        ResourceBundleThemeSource resourceBundleThemeSource = new ResourceBundleThemeSource();
        resourceBundleThemeSource.setBasenamePrefix(THEMES_BASENAME_PREFIX);
        return resourceBundleThemeSource;
    }

    @Bean
    public LocaleResolver localeResolver() {
        CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setDefaultLocale(Locale.ENGLISH);
        return cookieLocaleResolver;
    }
}
