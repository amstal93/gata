package com.odroid.gata.repository;

import com.odroid.gata.domain.Note;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NotesRepository extends JpaRepository<Note, Long> {

    @Query("select n from Note n "
            + "where "
            + "  ( lower(n.title) like lower(concat('%', :filter,'%')) "
            + "    or lower(n.content) like lower(concat('%', :filter, '%')) "
            + "  ) and n.user.username = :username "
            + "order by n.title ")
    Stream<Note> findNotes(@Param("filter") String filter, @Param("username") String username);
}
