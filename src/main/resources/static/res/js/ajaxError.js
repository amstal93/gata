var ajaxErrorController =
  gataApp.controller('ajaxErrorController', function ($scope, fw) {
    $scope.errorData = null;

    function onErrorAction(errorData) {
        $scope.errorData = errorData;
    }

    fw.ajax.onError(onErrorAction);

    $scope.hideError = function() {
        $scope.errorData = null;
    }

  });
