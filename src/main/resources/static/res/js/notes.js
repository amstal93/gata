var notesListController =
  gataApp.controller('notesListController', function ($scope, $window, $http, fw) {
    var NOTES_URL = "notes";
    var NOTES_REFRESH_TAG = "notesRefreshList";

    $scope.config = $window.config;

    function refreshList() {
        $scope.notes = [];
        loadMoreResults();
    }

    function loadMoreResults() {
      $scope.fetching = true;
      fw.ajax.nonConcurrentExec(NOTES_REFRESH_TAG, function () {
        $http.get(NOTES_URL, { params: { filter: $scope.filter, from: $scope.notes.length } })
          .then(function (response) {
            fw.ajax.nonConcurrentExecFinish(NOTES_REFRESH_TAG);
            response.data.forEach(
                function(elem) {
                    $scope.notes.push(elem);
                }
            );
            $scope.fetching = false;
          }, function(response) {
            fw.ajax.nonConcurrentExecFinishError(NOTES_REFRESH_TAG, response)
            $scope.fetching = false;
          }
        );
      })
    }

    function showNote(id) {
      $http.get(NOTES_URL + "/" + id)
        .then(function (response) {
          $scope.selectedNote = response.data;
        }, fw.ajax.httpError);
    }

    function unshowNote() {
      $scope.selectedNote = null;
    }

    $scope.$watch('filter', refreshList);

    $scope.refreshClicked = function () {
      refreshList();
    };

    $scope.cleanClicked = function () {
      $scope.filter = "";
    };

    $scope.addClicked = function () {
      $http.post(NOTES_URL)
        .then(function (response) {
          refreshList();
          showNote(response.data);
        }, fw.ajax.httpError);
    };

    $scope.noteClicked = function (id) {
      showNote(id);
    }

    $scope.saveClicked = function () {
      $http.put(NOTES_URL, $scope.selectedNote)
        .then(function (response) {
          refreshList();
        }, fw.ajax.httpError);
    };

    $scope.deleteClicked = function () {
      $http.delete(NOTES_URL + "/" + $scope.selectedNote.id)
        .then(function (response) {
          unshowNote();
          refreshList();
        }, fw.ajax.httpError);
    };

    $scope.loadNextPage = function() {
        loadMoreResults();
    }

    $scope.notes = [];
    $scope.filter = "";
    $scope.selectedNote = null;
    $scope.fetching = false;
  });
